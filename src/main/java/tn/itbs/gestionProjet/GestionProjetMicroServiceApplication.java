package tn.itbs.gestionProjet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class GestionProjetMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionProjetMicroServiceApplication.class, args);
	}

}
